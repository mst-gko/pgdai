# 1. Three WFS layers are imported to a dai schema in grukos db
# 2. An external SQL generates tables in schema mstdai
# 3. Views exposes data to MiljøGIS
import configparser
import os
import psycopg2
from osgeo import ogr
import logging
import datetime

DB_NAME = 'GRUKOS'
INI_FILE = 'F:/GKO/data/grukos/db_credentials/writer/writer.ini'

config = configparser.ConfigParser()
config.read(INI_FILE)

# DB credentials dictionary
DB_DICT = {
    'dbname': config[DB_NAME]['databasename'],
    'user': config[DB_NAME]['userid'],
    'password': config[DB_NAME]['password'],
    'host': config[DB_NAME]['host'],
    'port': config[DB_NAME]['port']
}


def setup_logger():
    # Get the directory of the script
    script_dir = os.path.dirname(os.path.abspath(__file__))

    # Create a "log" subfolder if it doesn't exist
    log_dir = os.path.join(script_dir, 'log')
    os.makedirs(log_dir, exist_ok=True)

    # Generate a timestamp for the log file name
    timestamp = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")

    # Create a logger
    logger = logging.getLogger('bnbo_log')
    logger.setLevel(logging.DEBUG)

    # Create a file handler with a timestamped log file in the "log" subfolder
    log_file = os.path.join(log_dir, f'log_{timestamp}.log')
    file_handler = logging.FileHandler(log_file)

    # Create a formatter
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    file_handler.setFormatter(formatter)

    # Add the file handler to the logger
    logger.addHandler(file_handler)

    return logger

def log_information(logger, message, print_log=True):
    # Log information to the logger
    logger.info(message)
    
    if print_log:
        print(message)
    
logger = setup_logger()
    
    
def execute_pgsql(sql):
    """
    Execute a SQL query on a PostGIS database.

    :param db_conn: Connection dictionary for the PostGIS database.
    :param sql: SQL query to be executed.
    """
    try:
        # Establish connection to the database
        conn = psycopg2.connect(**DB_DICT)

        # Create a cursor object
        cur = conn.cursor()

        # Execute the SQL query
        cur.execute(sql)

        # Commit the changes
        conn.commit()

        # Fetch the results if it's a select statement
        if sql.strip().lower().startswith('select'):
            results = cur.fetchall()
            cur.close()
            conn.close()
            return results

    except psycopg2.DatabaseError as e:
        log_information(logger, f'Database error: {e}')
    except Exception as e:
        log_information(logger, f'Error: {e}')
    finally:
        # Close the cursor and the connection
        if conn:
            cur.close()
            conn.close()


def sqlfile_to_sql(sql_filepath):
    """
    Read and returnse a SQL file as text.

    :param sql_filepath: Path to the SQL file to be executed.
    """
    try:
        # Check if the SQL file exists
        if not os.path.isfile(sql_filepath):
            log_information(logger, f"SQL file not found: {sql_filepath}")
            return

        # Read the SQL file
        with open(sql_filepath, 'r', encoding='utf-8') as sql_file:
            sql_content = sql_file.read()

            # Run SQL file using existing function
            return sql_content

    except IOError as e:
        log_information(logger, f"File error: {e}")
    except Exception as e:
        log_information(logger, f"Error: {e}")
        
        
def run_pg_sql_file(sql_filepath):
    """
    Read and execute a SQL file.

    :param sql_filepath: Path to the SQL file to be executed.
    """
    try:
        # Check if the SQL file exists
        if not os.path.isfile(sql_filepath):
            log_information(logger, f"SQL file not found: {sql_filepath}")
            return

        # Read the SQL file
        with open(sql_filepath, 'r') as sql_file:
            sql_content = sql_file.read()

            # Run SQL file using existing function
            execute_pgsql(sql_content)

    except IOError as e:
        log_information(logger, f"File error: {e}")
    except Exception as e:
        log_information(logger, f"Error: {e}")


url_info_list = [
    {'tablename': 'dmp_status_bnbo_aktuel', 'service': 'arealeditering-dist-geo.miljoeportal.dk/geoserver', 'typename': 'dai:status_bnbo'},
    {'tablename': 'dmp_aftarea_grvbes_aktuel', 'service': 'arealeditering-dist-geo.miljoeportal.dk/geoserver', 'typename': 'dai:aftarea_grvbes'},
    {'tablename': 'dmp_status_bnbo_historisk', 'service': 'b0902-prod-dist-app.azurewebsites.net/geoserver/dai_historik', 'typename': 'dai_historik:status_bnbo'}
]

def get_wfs_url(wfs_service, wfs_typename):
    # Test url strings:
    # dai_historik:status_bnbo  https://b0902-prod-dist-app.azurewebsites.net/geoserver/dai_historik/wfs?SERVICE=WFS&REQUEST=GetFeature&VERSION=2.0.0&TYPENAMES=dai_historik:status_bnbo&STARTINDEX=0&COUNT=20000&SRSNAME=urn:ogc:def:crs:EPSG::25832&BBOX=428995.86281796405091882,5858285.48015781678259373,933017.18049779045395553,6602841.56146626733243465,urn:ogc:def:crs:EPSG::25832
    # dai:status_bnbo           https://arealeditering-dist-geo.miljoeportal.dk/geoserver/wfs?SERVICE=WFS&REQUEST=GetFeature&VERSION=2.0.0&TYPENAMES=dai:status_bnbo&STARTINDEX=0&COUNT=20000&SRSNAME=urn:ogc:def:crs:EPSG::25832&BBOX=428995.86281796405091882,5858285.48015781678259373,933017.18049779045395553,6602841.56146626733243465,urn:ogc:def:crs:EPSG::25832
    # dai:aftarea_grvbes        https://arealeditering-dist-geo.miljoeportal.dk/geoserver/wfs?SERVICE=WFS&REQUEST=GetFeature&VERSION=2.0.0&TYPENAMES=dai:aftarea_grvbes&STARTINDEX=0&COUNT=20000&SRSNAME=urn:ogc:def:crs:EPSG::25832&BBOX=428995.86281796405091882,5858285.48015781678259373,933017.18049779045395553,6602841.56146626733243465,urn:ogc:def:crs:EPSG::25832
    
    url_wfs = f'https://{wfs_service}/wfs' \
              '?SERVICE=WFS' \
              '&REQUEST=GetFeature' \
              '&VERSION=2.0.0' \
              f'&TYPENAMES={wfs_typename}' \
              '&STARTINDEX=0' \
              '&COUNT=20000' \
              '&SRSNAME=urn:ogc:def:crs:EPSG::25832' \
              '&BBOX=428995.86281796405091882,' \
              '5858285.48015781678259373,' \
              '933017.18049779045395553,' \
              '6602841.56146626733243465,' \
              'urn:ogc:def:crs:EPSG::25832'

    return url_wfs
    
def execute_sql_ogr(pg_conn, sql_query):
    try:
        # Open a connection to the PostgreSQL database
        pg_conn_str = f"PG:host={pg_conn['host']} user={pg_conn['user']} dbname={pg_conn['dbname']} password={pg_conn['password']} port={pg_conn['port']}"
        connection = ogr.Open(pg_conn_str)

        if connection is None:
            raise Exception("Could not open a connection to the database")

        # Execute the SQL query without fetching results
        result = connection.ExecuteSQL(sql_query)

        if result is not None:
            # Log any result or message returned by ExecuteSQL, if applicable
            log_information(logger, "SQL query executed with a result set, indicating a possible SELECT statement or similar operation.")
            
        # Close the connection
        connection = None

        log_information(logger, "SQL query executed successfully.")
    except Exception as e:
        log_information(logger, f"Error: {str(e)}")
    
    
def wfs_to_postgis(wfs_url, pg_conn, target_table, overwrite=True):
    """
    Convert a WFS layer to a PostGIS table, with the layer being part of the WFS URL.

    :param wfs_url: URL to the WFS service, including the layer name.
    :param pg_conn: Connection dictionary for the PostGIS database.
    :param target_table: Name of the target table in PostGIS.
    """
    try:
        # Convert connection dictionary to OGR connection string
        pg_conn_str = f"PG:host={pg_conn['host']} user={pg_conn['user']} dbname={pg_conn['dbname']} password={pg_conn['password']} port={pg_conn['port']}"

        # Set up WFS driver and open WFS
        wfs_driver = ogr.GetDriverByName('WFS')
        wfs_ds = wfs_driver.Open(f'WFS:{wfs_url}')
        
        if wfs_ds is None:
            raise RuntimeError("Failed to open WFS.")

        # Assuming the first layer is the desired one as the layer name is in the URL
        source_layer = wfs_ds.GetLayerByIndex(0)

        if source_layer is None:
            raise RuntimeError("Layer not found in WFS.")

        # Set up PostGIS driver and open PostGIS database
        pg_driver = ogr.GetDriverByName('PostgreSQL')
        pg_ds = pg_driver.Open(pg_conn_str, 1)  # 1 is for update access

        if pg_ds is None:
            raise RuntimeError("Failed to connect to PostGIS database.")

        # Check if the target table exists
        existing_tables = pg_ds.ExecuteSQL(f"SELECT tablename FROM pg_tables WHERE schemaname = 'dai'")
        table_exists = any(row[0] == target_table for row in existing_tables)

        if table_exists:
            if overwrite:
                # If overwrite is enabled, delete the existing table
                pg_ds.DeleteLayer(f'dai.{target_table}')
            else:
                err = f"Table {target_table} already exists in the database. Set overwrite=True to replace it."
                log_information(logger, err)
                raise RuntimeError(err)
                
        # Copy layer to PostGIS
        pg_ds.CopyLayer(source_layer, f'dai.{target_table}')

    except Exception as e:
        log_information(logger, f"Error in wfs_to_postgis: {e}")
    finally:
        # Cleanup
        if wfs_ds:
            wfs_ds = None
        if pg_ds:
            pg_ds = None

    
    
def run_update(url_info_list, schemaname = 'dai'):
    try:
        for url_info in url_info_list:
            log_information(logger, f"Creating table via OGR2OGR {url_info['tablename']}...")
            
            # Get wfs url
            url = get_wfs_url(wfs_service=url_info['service'], wfs_typename=url_info['typename'])

            # create schemas if not exists
            sql_create_schema = '''
                CREATE SCHEMA IF NOT EXISTS dai;
                CREATE SCHEMA IF NOT EXISTS mstdai;
            '''
            execute_pgsql(sql=sql_create_schema)

            wfs_to_postgis(wfs_url=url, pg_conn=DB_DICT, target_table=url_info['tablename'])
            
            # Update table with timestamp now()
            sql_update_timestap = f"""
            	DO $$
				BEGIN
					IF NOT EXISTS (
						SELECT FROM information_schema.columns
						WHERE table_schema = 'dai'
						AND table_name = 'dai.{url_info['tablename']}'
						AND column_name = 'mst_datetime'
					) THEN
						ALTER TABLE dai.{url_info['tablename']}
						ADD COLUMN mst_datetime TIMESTAMP;
					ELSE
						RAISE NOTICE 'Column "mst_datetime" already exists in table "dmp_status_bnbo_aktuel".';
					END IF;

					UPDATE dai.{url_info['tablename']}
					SET mst_datetime = NOW();
				END
				$$;
            """
            
            #log_information(logger, sql_update_timestap, print_log=False)
            execute_pgsql(sql=sql_update_timestap)
                       
            # Grand rights
            sql_grants = '''
                GRANT USAGE ON SCHEMA dai TO grukosreader;
                GRANT SELECT ON ALL TABLES IN SCHEMA dai TO grukosreader; 
                GRANT USAGE ON SCHEMA mstdai TO grukosreader;
                GRANT SELECT ON ALL TABLES IN SCHEMA mstdai TO grukosreader; 
            '''
            execute_pgsql(sql=sql_grants)

        status_bnbo_sqlfile = os.path.join(f'{os.path.dirname(os.path.abspath(__file__))}', 'bnbo_status.sql')
        log_information(logger, f'Executing main sql from {status_bnbo_sqlfile}...')
        
        # Check if the SQL file exists
        if not os.path.isfile(status_bnbo_sqlfile):
            err = f"SQL file not found: {status_bnbo_sqlfile}"
            log_information(logger, err)
            raise FileNotFoundError(err)
        
        #run_pg_sql_file(status_bnbo_sqlfile)
        sql = sqlfile_to_sql(status_bnbo_sqlfile)
        execute_sql_ogr(DB_DICT, sql)
        
        log_information(logger, 'Update completed MIGHT BE successfully.')
    except Exception as e:
        err = f"Error in run_update: {e}"
        log_information(logger, err)
        raise FileNotFoundError(err)

run_update(url_info_list=url_info_list)