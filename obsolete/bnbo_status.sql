BEGIN;

CREATE SCHEMA IF NOT EXISTS mstdai;

/*
 * fix kommunenavn og dgunr i de aktuelle status_bnbo
 * der er en del boreholeno og kommunenavne der enten indenholder fejl eller er uhensigtsmaessigt skrevet ift. regex soegningen 
 * det var planen at disse skulle rettes igennem i gvkort.bnbo i 2023 da vi nu var faerdige med status_bnbo temaet, men...
 */
DROP TABLE IF EXISTS dmp_status_bnbo_aktuel_edit;

CREATE TEMP TABLE dmp_status_bnbo_aktuel_edit AS 
	(
		SELECT 
			CASE 
				WHEN a.kommunenav = 'Salten By Vandværk'
					THEN 'Silkeborg'
				WHEN a.kommunenav = 'Slagese'
					THEN 'Slagelse' 
				WHEN a.kommunenav = 'Slagelse kommune'
					THEN 'Slagelse' 
				WHEN a.kommunenav = 'Sønderborg Kommune'
					THEN 'Sønderborg' 
				WHEN a.kommunenav = 'Vordingborg Kommune'
					THEN 'Vordingborg' 
				WHEN a.kommunenav = 'Høje Tåstrup'
					THEN 'Høje-Taastrup' 	
				WHEN a.kommunenav = 'Frederikssund Kommune'
					THEN 'Frederikssund'
				WHEN a.kommunenav = 'Faxe Kommune'
					THEN 'Faxe' 
				WHEN a.kommunenav IS NULL
					AND objekt_id = '302699ad-b1b7-4fa8-80e8-0960ec1d29a1'
					THEN 'Guldborgsund'
				WHEN a.kommunenav IS NULL
					AND objekt_id = 'f2ee142b-890c-481d-a827-7a9ec240dfde'
					THEN 'Thisted' 
				WHEN a.kommunenav IS NULL
					AND objekt_id = '56a7b7d5-044c-4cb1-ad7d-154f1490d332'
					THEN 'Syddjurs'
				WHEN a.kommunenav = 'Rønbjerg Vandværk - Elkangård'
					THEN 'Vesthimmerlands'  
				WHEN a.kommunenav ILIKE '%Halsnæs%'
					THEN 'Halsnæs'
				WHEN a.kommunenav ILIKE '%Århus%' 
					THEN 'Aarhus' 
				WHEN a.kommunenav = 'Bornholms Region'
					THEN 'Bornholm' 
				WHEN a.kommunenav = 'Bornholms Regionskommune'
					THEN 'Bornholm' 
				WHEN a.kommunenav = 'Hovedstaden'
					THEN 'Høje-Taastrup' 
				ELSE a.kommunenav
				END AS kommunenav_edit,	
			dgunr AS dgunr_orig,
			CASE 
				WHEN dgunr = '186.851,1374,.1375,.1373,.1371,.1355,.1354,.1353'
					THEN '186.851,.1374,.1375,.1373,.1371,.1355,.1354,.1353'
				WHEN dgunr = '206.90X,.1154.1153,.90T,.1651,.90R,.90P,.90O'
					THEN '206.90X,.1154,.1153,.90T,.1651,.90R,.90P,.90O'
				WHEN dgunr = '170.385 170.21C'
					THEN '170.385, 170.21C'
				WHEN dgunr = '200.7001.7002.7003.7004.6988.6989.6987.6990.4223'
					THEN '200.7001,.7002,.7003,.7004,.6988,.6989,.6987,.6990,.4223'
				WHEN dgunr = '193.1585,.1588, 200.4344,.4345'
					THEN '193.1585, 193.1588, 200.4344, 200.4345'
				WHEN dgunr = '201.3770 mfl,3763,1445,1486,3675,3682,3676,1446'
					THEN '201.3770, 201.3763, 201.1445, 201.1486, 201.3675, 201.3682, 201.3676, 201.1446 mfl'
				WHEN dgunr = '206.1824, .1814, .934, .1059, 207.4317'
					THEN '206.1824, 206.1814, 206.934, 206.1059, 207.4317'
				WHEN dgunr = '199.995, 920, 950, 980 m.fl.'
					THEN '199.995, 199.920, 199.950, 199.980 m.fl.'
				WHEN dgunr = '200.6247,.6248,.6249,.6251, .6250, 3059'
					THEN '200.6247, 200.6248, 200.6249, 200.6251, 200.6250, 200.3059'
				WHEN dgunr = '200.7001.7002.7003.7004.6988.6989.6970.6990.4223'
					THEN '200.7001, 200.7002, 200.7003, 200.7004, 200.6988, 200.6989, 200.6970, 200.6990, 200.4223'
				WHEN dgunr = '199.1050; 199.'
					THEN '199.1050'
				WHEN dgunr = '199.741,742,743,744, 199.731,732, 199.388, 199.642'
					THEN '199.741, 199.742, 199.743, 199.744, 199.731,732, 199.388, 199.642'
				WHEN dgunr = '38.124,38.302,38.303,38.355, 38.430,460,461'
					THEN '38.124, 38.302, 38.303, 38.355, 38.430, 38.460, 38.461'
				WHEN dgunr = '146.171, 182, 532'
					THEN '146.171, 146.182, 146.532'
				WHEN dgunr = '200.6247,.6248,.6249,.6251,.6250, 3059'
					THEN '200.6247, 200.6248, 200.6249, 200.6251, 200.6250, 200.3059'
				WHEN dgunr = '87.790, 87,789'
					THEN '87.790, 87.789'
				WHEN dgunr = '87,1357,.1358,.1356,.1362,.1355,.1361,.1360,.1359'
					THEN '87.1357, 87.1358, 87.1356, 87.1362, 87.1355, 87.1361, 87.1360, 87.1359'
				WHEN dgunr = '79.009'
					THEN '71.442'
				WHEN dgunr IS NULL 
					AND objekt_id = '56a7b7d5-044c-4cb1-ad7d-154f1490d332'
					THEN '80.302'
				WHEN dgunr IS NULL 
					AND objekt_id = '302699ad-b1b7-4fa8-80e8-0960ec1d29a1'
					THEN '237.355'
				WHEN dgunr IS NULL 
					AND objekt_id = '1f88e55a-17b1-4d7d-b13a-e28619bf4245'
					THEN '207.2724, 207.2725, 207.2727, 207.3476'
				WHEN dgunr IS NULL 
					AND objekt_id = '57b05116-1680-48bd-b9c9-660ef10d88ee'
					THEN '200.4240, 200.4239, 200.4220, 200.4238, 200.4237, 200.4236'
				WHEN dgunr IS NULL 
					AND objekt_id = 'c93490d2-2b2c-4627-a2bf-a8c3ff432e88'
					THEN '57.816'
				WHEN dgunr IS NULL 
					AND objekt_id = '4f4fab5c-4af9-4b31-bbe8-dae62c98b8c8'
					THEN '117.251'
				WHEN dgunr IS NULL 
					AND objekt_id = '5502cd96-b004-4eed-b3db-6ebe825a5177'
					THEN '59.156'
				WHEN dgunr IS NULL 
					AND objekt_id = 'fe373d8c-6553-4f1f-b8e1-6ee0ef90c132'
					THEN '32.1074'
				WHEN dgunr IS NULL 
					AND objekt_id = '9f910db2-2adb-4961-97e1-e289079d3cfe'
					THEN '88.1048, 88.1306, 88.763, 88.1050, 88.1049'
				WHEN dgunr IS NULL 
					AND objekt_id = '4f4fab5c-4af9-4b31-bbe8-dae62c98b8c8'
					THEN '117.251'
				WHEN dgunr IS NULL 
					AND objekt_id = '40b029ec-76f0-4519-82f3-fc06ac23e625'
					THEN '206.1637'
				WHEN dgunr IS NULL 
					AND objekt_id = 'f6ece063-4297-440d-afad-b26f155099dc'
					THEN '200.4886, 200.3285, 200.2998'
				WHEN dgunr IS NULL 
					AND objekt_id = '1f88e55a-17b1-4d7d-b13a-e28619bf4245'
					THEN '207.2724, 207.2725, 207.2727, 207.3476'
				WHEN dgunr = '193.2148_'
					THEN '193.2148'
				ELSE dgunr
				END AS dgunr_edit,
			*
		FROM dai.dmp_status_bnbo_aktuel a 
	)
;

/*
 * fix kommunenavn og dgunr i de historiske status_bnbo
 * der er en del boreholeno og kommunenavne der enten indenholder fejl eller er uhensigtsmaessigt skrevet ift. regex soegningen 
 * det var planen at disse skulle rettes igennem i gvkort.bnbo i 2023 da vi nu var faerdige med status_bnbo temaet, men...
 */
DROP TABLE IF EXISTS dmp_status_bnbo_historisk_edit;

CREATE TEMP TABLE dmp_status_bnbo_historisk_edit AS 
	(
		SELECT 
			CASE 
				WHEN a.kommunenav = 'Salten By Vandværk'
					THEN 'Silkeborg'
				WHEN a.kommunenav = 'Slagese'
					THEN 'Slagelse' 
				WHEN a.kommunenav = 'Høje Tåstrup'
					THEN 'Høje-Taastrup' 	
				WHEN a.kommunenav = 'Frederikssund Kommune'
					THEN 'Frederikssund' 	
				WHEN a.kommunenav IS NULL
					AND objekt_id = '302699ad-b1b7-4fa8-80e8-0960ec1d29a1'
					THEN 'Guldborgsund'
				WHEN a.kommunenav IS NULL
					AND objekt_id = 'f2ee142b-890c-481d-a827-7a9ec240dfde'
					THEN 'Thisted' 
				WHEN a.kommunenav IS NULL
					AND objekt_id = '56a7b7d5-044c-4cb1-ad7d-154f1490d332'
					THEN 'Syddjurs'
				WHEN a.kommunenav = 'Rønbjerg Vandværk - Elkangård'
					THEN 'Vesthimmerlands'  
				WHEN a.kommunenav ILIKE '%Halsnæs%'
					THEN 'Halsnæs'
				WHEN a.kommunenav ILIKE '%Århus%' 
					THEN 'Aarhus' 
				WHEN a.kommunenav = 'Bornholms Region'
					THEN 'Bornholm' 
				WHEN a.kommunenav = 'Hovedstaden'
					THEN 'Høje-Taastrup' 
				ELSE a.kommunenav
				END AS kommunenav_edit,	
			dgunr AS dgunr_orig,
			CASE 
				WHEN dgunr = '186.851,1374,.1375,.1373,.1371,.1355,.1354,.1353'
					THEN '186.851,.1374,.1375,.1373,.1371,.1355,.1354,.1353'
				WHEN dgunr = '206.90X,.1154.1153,.90T,.1651,.90R,.90P,.90O'
					THEN '206.90X,.1154,.1153,.90T,.1651,.90R,.90P,.90O'
				WHEN dgunr = '170.385 170.21C'
					THEN '170.385, 170.21C'
				WHEN dgunr = '200.7001.7002.7003.7004.6988.6989.6987.6990.4223'
					THEN '200.7001,.7002,.7003,.7004,.6988,.6989,.6987,.6990,.4223'
				WHEN dgunr = '193.1585,.1588, 200.4344,.4345'
					THEN '193.1585, 193.1588, 200.4344, 200.4345'
				WHEN dgunr = '201.3770 mfl,3763,1445,1486,3675,3682,3676,1446'
					THEN '201.3770, 201.3763, 201.1445, 201.1486, 201.3675, 201.3682, 201.3676, 201.1446 mfl'
				WHEN dgunr = '206.1824, .1814, .934, .1059, 207.4317'
					THEN '206.1824, 206.1814, 206.934, 206.1059, 207.4317'
				WHEN dgunr = '199.995, 920, 950, 980 m.fl.'
					THEN '199.995, 199.920, 199.950, 199.980 m.fl.'
				WHEN dgunr = '200.6247,.6248,.6249,.6251, .6250, 3059'
					THEN '200.6247, 200.6248, 200.6249, 200.6251, 200.6250, 200.3059'
				WHEN dgunr = '200.7001.7002.7003.7004.6988.6989.6970.6990.4223'
					THEN '200.7001, 200.7002, 200.7003, 200.7004, 200.6988, 200.6989, 200.6970, 200.6990, 200.4223'
				WHEN dgunr = '199.1050; 199.'
					THEN '199.1050'
				WHEN dgunr = '199.741,742,743,744, 199.731,732, 199.388, 199.642'
					THEN '199.741, 199.742, 199.743, 199.744, 199.731,732, 199.388, 199.642'
				WHEN dgunr = '38.124,38.302,38.303,38.355, 38.430,460,461'
					THEN '38.124, 38.302, 38.303, 38.355, 38.430, 38.460, 38.461'
				WHEN dgunr = '146.171, 182, 532'
					THEN '146.171, 146.182, 146.532'
				WHEN dgunr = '200.6247,.6248,.6249,.6251,.6250, 3059'
					THEN '200.6247, 200.6248, 200.6249, 200.6251, 200.6250, 200.3059'
				WHEN dgunr = '87.790, 87,789'
					THEN '87.790, 87.789'
				WHEN dgunr = '87,1357,.1358,.1356,.1362,.1355,.1361,.1360,.1359'
					THEN '87.1357, 87.1358, 87.1356, 87.1362, 87.1355, 87.1361, 87.1360, 87.1359'
				WHEN dgunr = '79.009'
					THEN '71.442'
				WHEN dgunr IS NULL 
					AND objekt_id = '56a7b7d5-044c-4cb1-ad7d-154f1490d332'
					THEN '80.302'
				WHEN dgunr IS NULL 
					AND objekt_id = '302699ad-b1b7-4fa8-80e8-0960ec1d29a1'
					THEN '237.355'
				WHEN dgunr IS NULL 
					AND objekt_id = '1f88e55a-17b1-4d7d-b13a-e28619bf4245'
					THEN '207.2724, 207.2725, 207.2727, 207.3476'
				WHEN dgunr IS NULL 
					AND objekt_id = '57b05116-1680-48bd-b9c9-660ef10d88ee'
					THEN '200.4240, 200.4239, 200.4220, 200.4238, 200.4237, 200.4236'
				WHEN dgunr IS NULL 
					AND objekt_id = 'c93490d2-2b2c-4627-a2bf-a8c3ff432e88'
					THEN '57.816'
				WHEN dgunr IS NULL 
					AND objekt_id = '4f4fab5c-4af9-4b31-bbe8-dae62c98b8c8'
					THEN '117.251'
				WHEN dgunr IS NULL 
					AND objekt_id = '5502cd96-b004-4eed-b3db-6ebe825a5177'
					THEN '59.156'
				WHEN dgunr IS NULL 
					AND objekt_id = 'fe373d8c-6553-4f1f-b8e1-6ee0ef90c132'
					THEN '32.1074'
				WHEN dgunr IS NULL 
					AND objekt_id = '9f910db2-2adb-4961-97e1-e289079d3cfe'
					THEN '88.1048, 88.1306, 88.763, 88.1050, 88.1049'
				WHEN dgunr IS NULL 
					AND objekt_id = '4f4fab5c-4af9-4b31-bbe8-dae62c98b8c8'
					THEN '117.251'
				WHEN dgunr IS NULL 
					AND objekt_id = '40b029ec-76f0-4519-82f3-fc06ac23e625'
					THEN '206.1637'
				WHEN dgunr IS NULL 
					AND objekt_id = 'f6ece063-4297-440d-afad-b26f155099dc'
					THEN '200.4886, 200.3285, 200.2998'
				WHEN dgunr IS NULL 
					AND objekt_id = '1f88e55a-17b1-4d7d-b13a-e28619bf4245'
					THEN '207.2724, 207.2725, 207.2727, 207.3476'
				WHEN dgunr = '193.2148_'
					THEN '193.2148'
				ELSE dgunr
				END AS dgunr_edit,
			*
		FROM dai.dmp_status_bnbo_historisk a 
	)
;

/*
 * find de enkelte dgunumre fra "dgunr" kolonnen i de aktuelle status_bnbo
 */
DROP TABLE IF EXISTS status_bnbo_borehole_aktuel;

CREATE TEMP TABLE status_bnbo_borehole_aktuel AS 
	(
		WITH 
			-- aktuel_bnbo_2
			-- erstatter underscore med punktum og ændre alle bokstaver til stort
			aktuel_bnbo_2 AS
				(
					SELECT 
						objekt_id,
						dgunr_orig,
						upper(REPLACE(dgunr_edit, '_', '.')) AS dgunr
					FROM dmp_status_bnbo_aktuel_edit
				),
			 -- aktuel_bnbo_3
			 -- dgu1 er tallene for punktum i dgunr fx ved '100.100, .101' => '100.'. 
			 -- dgu2 er tallene og evt. et bokstav efter punktum fx '100.100, .101' => '.100' OG '.101'.
			 -- bemaerk at foeste del af regex string i dgu2 frasortere loesninger med punktum efter tal, altsaa så vi sikre at dgu1 ikke kan medtages i dgu2.
			 -- 'g' i regexp_matches giver global soegning med alle matches frem for foerste
			 -- hver dgunr bliver ligeledes tildelt et objekt nummer
			aktuel_bnbo_3 AS 
				(
					SELECT DISTINCT
						tmp1.dgunr, 
						tmp1.objekt_id,
						tmp2.object_num,
						tmp2.dgu1,
						tmp2.dgu2
					FROM aktuel_bnbo_2 tmp1
					LEFT JOIN 
						(
							SELECT DISTINCT 
								objekt_id, 
								ROW_NUMBER() OVER () AS object_num, 
								REGEXP_MATCHES(dgunr, '([0-9]+)\s*(\.)', 'g') AS dgu1,
								REGEXP_MATCHES(dgunr, '(?!\.[0-9]+\.)(\.)\s*([0-9]+[a-zA-Z]?)', 'g') AS dgu2
							FROM aktuel_bnbo_2 tmp1
						) AS tmp2 USING (objekt_id)
				),
			-- aktuel_bnbo_4
			-- for at kunne sortere flere regex resultater for hver objekt nummer, tildeles de et versions nummer.
			-- hvis dgu1 kun en tom string aendres denne til null, for at coalesce funktionen i aktuel_bnbo_5 fungere
			aktuel_bnbo_4 AS 
				(
					SELECT 
						objekt_id,
						dgunr, 
						object_num, 
						ROW_NUMBER() OVER (PARTITION BY object_num ORDER BY dgu1[1], dgu2[2]) AS version_num, 
						NULLIF(dgu1[1], '') AS dgu1, 
						dgu2[2] AS dgu2
					FROM aktuel_bnbo_3
				),
			-- aktuel_bnbo_5
			-- maalet er at få fx '100.100, .101' til '100.100' OG '100.101'
			-- den bedst praesterende metode jeg kunne finde, var at lave et coalesce funktion,
			-- der findes foeste ikke null. input er adskillige lag funktioner der proever at finde en dgu1 for samme objekt nummer
			-- her tages en en raekke over, så 2 raekker over osv. opdelt efter versions nummeret.
			-- grunden til at versions nummeret er noedvendigt, er at nogle dgunr har formen '100.100, .101, 200.103' => '100.100' OG '100.101' OG '200.103'
			aktuel_bnbo_5 AS 
				(
					SELECT 
						objekt_id, dgunr, 
						object_num, version_num, 
						COALESCE(
							dgu1,
							LAG(dgu1, 1) OVER (PARTITION BY object_num ORDER BY version_num), 
							LAG(dgu1, 2) OVER (PARTITION BY object_num ORDER BY version_num),
							LAG(dgu1, 3) OVER (PARTITION BY object_num ORDER BY version_num), 
							LAG(dgu1, 4) OVER (PARTITION BY object_num ORDER BY version_num),
							LAG(dgu1, 5) OVER (PARTITION BY object_num ORDER BY version_num), 
							LAG(dgu1, 6) OVER (PARTITION BY object_num ORDER BY version_num),
							LAG(dgu1, 7) OVER (PARTITION BY object_num ORDER BY version_num), 
							LAG(dgu1, 8) OVER (PARTITION BY object_num ORDER BY version_num),
							LAG(dgu1, 9) OVER (PARTITION BY object_num ORDER BY version_num), 
							LAG(dgu1, 10) OVER (PARTITION BY object_num ORDER BY version_num),
							LAG(dgu1, 11) OVER (PARTITION BY object_num ORDER BY version_num), 
							LAG(dgu1, 12) OVER (PARTITION BY object_num ORDER BY version_num),
							LAG(dgu1, 13) OVER (PARTITION BY object_num ORDER BY version_num), 
							LAG(dgu1, 14) OVER (PARTITION BY object_num ORDER BY version_num),
							LAG(dgu1, 15) OVER (PARTITION BY object_num ORDER BY version_num), 
							LAG(dgu1, 16) OVER (PARTITION BY object_num ORDER BY version_num),
							LAG(dgu1, 17) OVER (PARTITION BY object_num ORDER BY version_num), 
							LAG(dgu1, 18) OVER (PARTITION BY object_num ORDER BY version_num),
							LAG(dgu1, 19) OVER (PARTITION BY object_num ORDER BY version_num), 
							LAG(dgu1, 20) OVER (PARTITION BY object_num ORDER BY version_num)
							) AS dgu1, 
						dgu2
					FROM aktuel_bnbo_4
					ORDER BY object_num, version_num DESC 
				),
			-- aktuel_bnbo_6
			-- her sammensaettes dgu1 og dgu2 til et samlet dgunr
			-- her vil et samlet dgunr med flere boringer få en post for hver boring der trækkes ud
			aktuel_bnbo_6 AS 
				(
					SELECT
						b1.kommunenav_edit,
						b1.dgunr_orig, 
						b1.objekt_id, 
						b5.dgu1 || '.' || b5.dgu2 AS dgunr_edit
					FROM dmp_status_bnbo_aktuel_edit b1
					LEFT JOIN aktuel_bnbo_5 b5 USING (objekt_id)
				)
		-- udelukkende for at goere det lettere at fejlfinde i koden, saaledes at hver CTE query kan koeres her
		SELECT *
		FROM aktuel_bnbo_6
	)
;

/*
 * find de enkelte dgunumre fra "dgunr" kolonnen i de historiske status_bnbo
 */
DROP TABLE IF EXISTS status_bnbo_borehole_historisk;

CREATE TEMP TABLE status_bnbo_borehole_historisk AS
	(
		WITH 
			-- historisk_bnbo_2
			-- erstatter underscore med punktum og ændre alle bokstaver til stort
			historisk_bnbo_2 AS
				(
					SELECT 
						objekt_id,
						dgunr_orig,
						upper(REPLACE(dgunr_edit, '_', '.')) AS dgunr
					FROM dmp_status_bnbo_historisk_edit
				),
			-- historisk_bnbo_3
			-- dgu1 er tallene for punktum i dgunr fx ved '100.100, .101' => '100.'. 
			-- dgu2 er tallene og evt. et bokstav efter punktum fx '100.100, .101' => '.100' OG '.101'.
			-- bemaerk at foeste del af regex string i dgu2 frasortere loesninger med punktum efter tal, altsaa så vi sikre at dgu1 ikke kan medtages i dgu2.
			-- 'g' i regexp_matches giver global soegning med alle matches frem for foerste
			-- hver dgunr bliver ligeledes tildelt et objekt nummer
			historisk_bnbo_3 AS 
				(
					SELECT DISTINCT
						tmp1.dgunr, 
						tmp1.objekt_id,
						tmp2.object_num,
						tmp2.dgu1,
						tmp2.dgu2
					FROM historisk_bnbo_2 tmp1
					LEFT JOIN 
						(
							SELECT DISTINCT
								objekt_id, 
								ROW_NUMBER() OVER () AS object_num, 
								REGEXP_MATCHES(dgunr, '([0-9]+)\s*(\.)', 'g') AS dgu1,
								REGEXP_MATCHES(dgunr, '(?!\.[0-9]+\.)(\.)\s*([0-9]+[a-zA-Z]?)', 'g') AS dgu2
							FROM historisk_bnbo_2 tmp1
						) AS tmp2 USING (objekt_id)
				),
			-- historisk_bnbo_4
			-- for at kunne sortere flere regex resultater for hver objekt nummer, tildeles de et versions nummer.
			-- hvis dgu1 kun en tom string aendres denne til null, for at coalesce funktionen i aktuel_bnbo_5 fungere
			historisk_bnbo_4 AS 
				(
					SELECT 
						objekt_id,
						dgunr, 
						object_num, 
						ROW_NUMBER() OVER (PARTITION BY object_num ORDER BY dgu1[1], dgu2[2]) AS version_num, 
						NULLIF(dgu1[1], '') AS dgu1, 
						dgu2[2] AS dgu2
					FROM historisk_bnbo_3
				),
			-- historisk_bnbo_5
			-- maalet er at få fx '100.100, .101' til '100.100' OG '100.101'
			-- den bedst praesterende metode jeg kunne finde, var at lave et coalesce funktion,
			-- der findes foeste ikke null. input er adskillige lag funktioner der proever at finde en dgu1 for samme objekt nummer
			-- her tages en en raekke over, så 2 raekker over osv. opdelt efter versions nummeret.
			-- grunden til at versions nummeret er noedvendigt, er at nogle dgunr har formen '100.100, .101, 200.103' => '100.100' OG '100.101' OG '200.103'
			historisk_bnbo_5 AS 
				(
					SELECT 
						objekt_id, dgunr, 
						object_num, version_num, 
						COALESCE(
							dgu1,
							LAG(dgu1, 1) OVER (PARTITION BY object_num ORDER BY version_num), 
							LAG(dgu1, 2) OVER (PARTITION BY object_num ORDER BY version_num),
							LAG(dgu1, 3) OVER (PARTITION BY object_num ORDER BY version_num), 
							LAG(dgu1, 4) OVER (PARTITION BY object_num ORDER BY version_num),
							LAG(dgu1, 5) OVER (PARTITION BY object_num ORDER BY version_num), 
							LAG(dgu1, 6) OVER (PARTITION BY object_num ORDER BY version_num),
							LAG(dgu1, 7) OVER (PARTITION BY object_num ORDER BY version_num), 
							LAG(dgu1, 8) OVER (PARTITION BY object_num ORDER BY version_num),
							LAG(dgu1, 9) OVER (PARTITION BY object_num ORDER BY version_num), 
							LAG(dgu1, 10) OVER (PARTITION BY object_num ORDER BY version_num),
							LAG(dgu1, 11) OVER (PARTITION BY object_num ORDER BY version_num), 
							LAG(dgu1, 12) OVER (PARTITION BY object_num ORDER BY version_num),
							LAG(dgu1, 13) OVER (PARTITION BY object_num ORDER BY version_num), 
							LAG(dgu1, 14) OVER (PARTITION BY object_num ORDER BY version_num),
							LAG(dgu1, 15) OVER (PARTITION BY object_num ORDER BY version_num), 
							LAG(dgu1, 16) OVER (PARTITION BY object_num ORDER BY version_num),
							LAG(dgu1, 17) OVER (PARTITION BY object_num ORDER BY version_num), 
							LAG(dgu1, 18) OVER (PARTITION BY object_num ORDER BY version_num),
							LAG(dgu1, 19) OVER (PARTITION BY object_num ORDER BY version_num), 
							LAG(dgu1, 20) OVER (PARTITION BY object_num ORDER BY version_num)
							) AS dgu1, 
						dgu2
					FROM historisk_bnbo_4
					ORDER BY object_num, version_num DESC 
				),
			-- historisk_bnbo_6
			-- her sammensaettes dgu1 og dgu2 til et samlet dgunr
			-- her vil et samlet dgunr med flere boringer få en post for hver boring der trækkes ud
			historisk_bnbo_6 AS 
				(
					SELECT
						b1.kommunenav,
						b1.dgunr_orig, 
						b1.objekt_id, 
						b5.dgu1 || '.' || b5.dgu2 AS dgunr_edit
					FROM dmp_status_bnbo_historisk_edit b1
					LEFT JOIN historisk_bnbo_5 b5 USING (objekt_id)
				)
		-- udelukkende for at goere det lettere at fejlfinde i koden, saaledes at hver CTE query kan koeres her
		SELECT DISTINCT 
			*
		FROM historisk_bnbo_6
	)
;

/* 
 * lav en oversætter funktion mellem aktuelle og historiske bnbo
 * dette tabel kunne sagtens undvaeres og status_bnbo_aktuel_historisk koeres direkte,
 * men jeg kan godt lide mange mellemregninger for at sikre at jeg har styr på data hele vejen.
 */
DROP TABLE IF EXISTS status_bnbo_transfer_aktuel_historisk;

CREATE TEMP TABLE status_bnbo_transfer_aktuel_historisk AS 
	(
		-- combine
		-- her forsoeges at samle det aktuelle og historiske status_bnbo tema
		-- dette goeres foest ved at join via 1) originale dgunr, 2) det editerede dgunr og 3) det sammensatte regex dgunr
		-- dette medfører at der bliver dubletter hvis begge fx har dgunr = '100.100'
		WITH combine AS 
			(
				SELECT  
					a.*,
					h1.objekt_id AS objektid_hist,
					h1.dgunr_orig AS dgunr_orig_hist,
					h1.dgunr_edit AS dgunr_edit_hist
				FROM status_bnbo_borehole_aktuel a
				LEFT JOIN status_bnbo_borehole_historisk h1 ON a.dgunr_orig = h1.dgunr_orig 
				UNION ALL 
				SELECT  
					a.*,
					h2.objekt_id AS objektid_hist,
					h2.dgunr_orig AS dgunr_orig_hist,
					h2.dgunr_edit AS dgunr_edit_hist
				FROM status_bnbo_borehole_aktuel a
				LEFT JOIN status_bnbo_borehole_historisk h2 ON a.dgunr_edit = h2.dgunr_edit 
				UNION ALL 
				SELECT  
					a.*,
					h3.objekt_id AS objektid_hist,
					h3.dgunr_orig AS dgunr_orig_hist,
					h3.dgunr_edit AS dgunr_edit_hist
				FROM status_bnbo_borehole_aktuel a
				LEFT JOIN status_bnbo_borehole_historisk h3 ON a.objekt_id = h3.objekt_id 
			)
		-- da vi koere flere joins i combine, koeres en distint her saa et aktuel og et historisk kun bliver samlet en gang
		SELECT DISTINCT 
			objekt_id,
			dgunr_edit, 
			dgunr_orig, 
			objektid_hist,
			dgunr_orig_hist,
			dgunr_edit_hist
		FROM combine
		WHERE objektid_hist IS NOT NULL 
	)
;

/*
 * saml aktuel og historisk bnbo ud fra oversaetter funktionen
 */
DROP TABLE IF EXISTS mst_status_bnbo_compare_aktuel_historisk_temp CASCADE;

CREATE TEMP TABLE mst_status_bnbo_compare_aktuel_historisk_temp AS 
	(
		WITH 
			-- hist_grp
			-- her grupperes status for det originale historiske tema ud fra objekt_id
			-- herved opnaas alle tidigere status et paagaeldende bnbo har haft gennem tiden.
			hist_grp AS 
				(
					SELECT 
						h.objekt_id, 
						string_agg(DISTINCT h.status_bnbo, ' | ') AS status_bnbo_hist_grp
					FROM dai.dmp_status_bnbo_historisk h
					GROUP BY h.objekt_id 
				)
		-- status_bnbo er fra det aktuelle tema.
		-- status_bnbo_hist_grp er den grupperede historiske status.
		-- status_bnbo_combined er primært det aktuelle, men historisk er indsat hvis det aktuelle havde default status 
		-- og det historiske havde en status forskellig fra default. Fra det historiske tema hentes den seneste ikke default status.
		SELECT DISTINCT ON (a.objekt_id)
			now() AS mst_datetime,
			a.kommunenav_edit,
			a.objekt_id, 
			--bb.id,
			a.dgunr,
			a.dgunr_edit,
			a.status_bnbo,
			COALESCE(NULLIF(a.status_bnbo, 'Ikke gennemgået (default værdi)'), h.status_bnbo, a.status_bnbo) AS status_bnbo_combined,
			h.dgunr AS dgunr_hist,
			h.dgunr_edit AS dgunr_edit_hist,
			status_bnbo_hist_grp,
			st_area(a.wkb_geometry) * 0.0001 AS bnbo_area_ha,
			ST_CurveToLine(a.wkb_geometry) AS geom,
			CONCAT('https://mst.dk/erhverv/rent-miljoe-og-sikker-forsyning/drikkevand-og-grundvand/grundvandskortlaegning/kortlaegningsresultater/',REPLACE(REPLACE(REPLACE(a.kommunenav_edit,'æ','ae'),'ø','oe'),'å','aa'),'-kommune') as url
		FROM dmp_status_bnbo_aktuel_edit a
		LEFT JOIN status_bnbo_transfer_aktuel_historisk t ON t.objekt_id = a.objekt_id
		LEFT JOIN dmp_status_bnbo_historisk_edit h ON t.objektid_hist = h.objekt_id
		LEFT JOIN hist_grp hg ON h.objekt_id = hg.objekt_id
		ORDER BY a.objekt_id, CASE WHEN h.status_bnbo = 'Ikke gennemgået (default værdi)' THEN 2 ELSE 1 END, h.systid_til DESC NULLS LAST 
	)
;

DROP TABLE IF EXISTS mst_antal_bnbo_med_aftareal_temp;

CREATE TEMP TABLE mst_antal_bnbo_med_aftareal_temp AS 
	(
		WITH
		status_count_all AS 
			(
				SELECT 
					kommunenav_edit,
					count(*) AS bnbo_total
				FROM mst_status_bnbo_compare_aktuel_historisk_temp
				GROUP BY kommunenav_edit
			),
		udv_status AS 
			(
				SELECT 
					*
				FROM mst_status_bnbo_compare_aktuel_historisk_temp
				WHERE status_bnbo_combined IN ('Gennemgået, indsats nødvendig', 'Frivillig aftale tilbudt (UDGÅET)', 'Indsats gennemført') 
			),
		status_bnbo_count_all AS 
			(
				SELECT 
					kommunenav_edit,
					count(*) AS bnbo_udvalgt
				FROM udv_status
				GROUP BY kommunenav_edit
			),
		aft_area AS 
			(
				SELECT st_union(ST_CurveToLine(wkb_geometry)) AS geom  --psycopg2 Unknown geometry type: 12 - Use ST_CurveToLine
				FROM dai.dmp_aftarea_grvbes_aktuel
			),
		status_bnbo_aft_area AS 
			(
				SELECT 
					sb.kommunenav_edit,
					SUM(
						CASE 
							WHEN aa.geom IS NOT NULL 
								THEN 1
							ELSE 0
						END 
					) AS bnbo_udvalgt_med_aftarea
				FROM udv_status sb
				LEFT JOIN aft_area aa ON st_intersects(ST_CurveToLine(sb.geom), ST_CurveToLine(aa.geom))  --psycopg2 Unknown geometry type: 12 - Use ST_CurveToLine
				GROUP BY kommunenav_edit 
			)
		SELECT 
			now() AS mst_datetime,
			k.komnavn AS kommunenav_edit,
			COALESCE(bnbo_total, 0) AS antal_alle,
			COALESCE(bnbo_udvalgt, 0) AS antal_udvalgte_3_status,
			COALESCE(bnbo_udvalgt_med_aftarea, 0) AS antal_udvalgte_3_status_aftareal,
			round(100 * bnbo_udvalgt_med_aftarea::NUMERIC / bnbo_udvalgt::NUMERIC, 2) AS antal_udvalgte_3_status_aftareal_pct,
			k.geom
		FROM kort.kommunegraense k 
		LEFT JOIN status_bnbo_aft_area sb ON sb.kommunenav_edit = k.komnavn
		LEFT JOIN status_bnbo_count_all cba ON cba.kommunenav_edit = k.komnavn
		LEFT JOIN status_count_all ca ON ca.kommunenav_edit = k.komnavn
		ORDER BY komnavn
	)
;

DROP TABLE IF EXISTS mst_antal_bnbo_forskellig_status_temp;

CREATE TEMP TABLE mst_antal_bnbo_forskellig_status_temp AS 
	(
		WITH 
		bnbo_all AS 
			(
				SELECT *
				FROM mst_status_bnbo_compare_aktuel_historisk_temp
			),
		count_udvalg1 AS 
			(
				SELECT 
					kommunenav_edit, 
					count(*) AS count_udvalgte_3_status 
				FROM bnbo_all
				WHERE status_bnbo_combined IN ('Gennemgået, indsats nødvendig', 'Frivillig aftale tilbudt (UDGÅET)', 'Indsats gennemført') 
				GROUP BY kommunenav_edit
			),
		count_udvalg2 AS 
			(
				SELECT 
					kommunenav_edit, 
					count(*) AS count_udvalgte_2_status
				FROM bnbo_all
				WHERE status_bnbo_combined IN ('Frivillig aftale tilbudt (UDGÅET)', 'Indsats gennemført') 
				GROUP BY kommunenav_edit
			),
		count_udvalg3 AS 
			(
				SELECT 
					kommunenav_edit, 
					count(*) AS count_alle 
				FROM bnbo_all
				GROUP BY kommunenav_edit
			),
		count_udvalg4 AS 
			(
				SELECT 
					kommunenav_edit, 
					count(*) AS count_udvalgte_1_status
				FROM bnbo_all
				WHERE status_bnbo_combined IN ('Indsats gennemført') 
				GROUP BY kommunenav_edit
			),
		count_udvalg_default_status AS 
			(
				SELECT 
					kommunenav_edit, 
					count(*) AS count_default_status
				FROM bnbo_all
				WHERE  status_bnbo_combined IN ('Ikke gennemgået (default værdi)')
				GROUP BY kommunenav_edit
			)
		SELECT 
			now() AS mst_datetime,
			k.komnavn AS kommunenav_edit,
			COALESCE(count_alle, 0) AS antal_alle,
			CASE
				WHEN COALESCE(count_alle, 0) = 0
				THEN NULL
				ELSE COALESCE(count_udvalgte_3_status, 0) 
			END AS antal_udvalgte_3_status,
			CASE
				WHEN COALESCE(count_alle, 0) = 0
				THEN NULL
				ELSE COALESCE(count_udvalgte_2_status, 0) 
			END AS antal_udvalgte_2_status,
			CASE
				WHEN COALESCE(count_alle, 0) = 0
				THEN NULL
				ELSE COALESCE(count_udvalgte_1_status, 0) 
			END AS antal_udvalgte_1_status,
			COALESCE(count_default_status, 0) AS antal_default_status,
			CASE
				WHEN COALESCE(count_alle, 0) = 0
				THEN NULL
				WHEN COALESCE(count_udvalgte_3_status, 0) = 0
				THEN 999
				ELSE 100 * COALESCE(count_udvalgte_2_status, 0) / count_udvalgte_3_status 
			END AS pct_2af3,
			CASE
				WHEN COALESCE(count_alle, 0) = 0
				THEN NULL
				WHEN COALESCE(count_udvalgte_3_status, 0) = 0
				THEN 999
				ELSE 100 * COALESCE(count_udvalgte_1_status, 0) / count_udvalgte_3_status 
			END AS pct_1af3,
			k.geom
		FROM kort.kommunegraense k 
		LEFT JOIN count_udvalg4 c4 ON k.komnavn = c4.kommunenav_edit
		LEFT JOIN count_udvalg3 c3 ON k.komnavn = c3.kommunenav_edit
		LEFT JOIN count_udvalg2 c2 ON k.komnavn = c2.kommunenav_edit
		LEFT JOIN count_udvalg1 c1  ON k.komnavn = c1.kommunenav_edit
		LEFT JOIN count_udvalg_default_status d ON k.komnavn = d.kommunenav_edit
	)
;

DROP TABLE IF EXISTS mstdai.vw_grukos2miljoegis_bnbostatus_historisk_aktuel_sammenligning CASCADE;

CREATE TABLE mstdai.vw_grukos2miljoegis_bnbostatus_historisk_aktuel_sammenligning AS 
	(
		SELECT 
			*
		FROM mst_status_bnbo_compare_aktuel_historisk_temp
	)
;

CREATE INDEX vw_grukos2miljoegis_bnbostatus_historisk_aktuel_sammenligning_idx 
	ON mstdai.vw_grukos2miljoegis_bnbostatus_historisk_aktuel_sammenligning
	USING GIST (geom)
;

DROP TABLE IF EXISTS mstdai.mst_antal_bnbo_med_aftareal;

CREATE TABLE mstdai.mst_antal_bnbo_med_aftareal AS 
	(
		SELECT 
			*
		FROM mst_antal_bnbo_med_aftareal_temp
	)
;

CREATE INDEX mst_antal_bnbo_med_aftareal_geometry_idx 
	ON mstdai.mst_antal_bnbo_med_aftareal
	USING GIST (geom)
;

DROP TABLE IF EXISTS mstdai.vw_grukos2miljoegis_bnbo_statusopgoerelse CASCADE;

CREATE TABLE mstdai.vw_grukos2miljoegis_bnbo_statusopgoerelse AS 
	(
		SELECT 
			*
		FROM mst_antal_bnbo_forskellig_status_temp
	)
;

CREATE INDEX vw_grukos2miljoegis_bnbo_statusopgoerelse_idx 
	ON mstdai.vw_grukos2miljoegis_bnbo_statusopgoerelse
	USING GIST (geom)
;

GRANT USAGE ON SCHEMA dai TO grukosreader;
		
GRANT SELECT ON ALL TABLES IN SCHEMA dai TO grukosreader; 

GRANT USAGE ON SCHEMA mstdai TO grukosreader;
		
GRANT SELECT ON ALL TABLES IN SCHEMA mstdai TO grukosreader; 


CREATE OR REPLACE VIEW gvkort.vw_grukos2miljoegis_bnbostatus_historisk_aktuel_sammenligning
AS SELECT 
	*,
	row_number() OVER (ORDER BY objekt_id) AS id
   FROM mstdai.vw_grukos2miljoegis_bnbostatus_historisk_aktuel_sammenligning msbcah 
;
alter table gvkort.vw_grukos2miljoegis_bnbostatus_historisk_aktuel_sammenligning owner to grukosadmin;
grant select on gvkort.vw_grukos2miljoegis_bnbostatus_historisk_aktuel_sammenligning to dmp_reader;
grant select on gvkort.vw_grukos2miljoegis_bnbostatus_historisk_aktuel_sammenligning to grukosreader;
grant select on gvkort.vw_grukos2miljoegis_bnbostatus_historisk_aktuel_sammenligning to jupiterreader;
grant select on gvkort.vw_grukos2miljoegis_bnbostatus_historisk_aktuel_sammenligning to jupiterrole;
grant select on gvkort.vw_grukos2miljoegis_bnbostatus_historisk_aktuel_sammenligning to miljoegis_grukos_reader;
grant select on gvkort.vw_grukos2miljoegis_bnbostatus_historisk_aktuel_sammenligning to grukosbek;
grant select on gvkort.vw_grukos2miljoegis_bnbostatus_historisk_aktuel_sammenligning to miljoegis_grukos_reader_test;

CREATE OR REPLACE VIEW gvkort.vw_grukos2miljoegis_bnbo_statusopgoerelse
AS SELECT 
	*,
	row_number() OVER (ORDER BY geom) AS id
   FROM mstdai.vw_grukos2miljoegis_bnbo_statusopgoerelse
;
alter table gvkort.vw_grukos2miljoegis_bnbo_statusopgoerelse owner to grukosadmin;
grant select on gvkort.vw_grukos2miljoegis_bnbo_statusopgoerelse to dmp_reader;
grant select on gvkort.vw_grukos2miljoegis_bnbo_statusopgoerelse to grukosreader;
grant select on gvkort.vw_grukos2miljoegis_bnbo_statusopgoerelse to jupiterreader;
grant select on gvkort.vw_grukos2miljoegis_bnbo_statusopgoerelse to jupiterrole;
grant select on gvkort.vw_grukos2miljoegis_bnbo_statusopgoerelse to miljoegis_grukos_reader;
grant select on gvkort.vw_grukos2miljoegis_bnbo_statusopgoerelse to grukosbek;
grant select on gvkort.vw_grukos2miljoegis_bnbo_statusopgoerelse to miljoegis_grukos_reader_test;

COMMIT;